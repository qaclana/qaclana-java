= Qaclana Processors - SQL Injection processor

This is a definitive negative processor: it matches the request parameters to a set of regular expressions for signs of
SQL Injection attacks. This acts only on the request, so, a `NEUTRAL` outcome for the response phase is always returned.