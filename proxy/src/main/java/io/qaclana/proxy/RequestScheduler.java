/*
 * Copyright 2016 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.proxy;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.inject.Inject;
import javax.servlet.AsyncContext;

import io.qaclana.settings.SettingsValue;

/**
 * @author Juraci Paixão Kröhling
 */
@Stateless
public class RequestScheduler {
    private static final MsgLogger logger = MsgLogger.LOGGER;

    @Inject
    @SettingsValue("qaclana.proxy.backend")
    String backingServer;

    @SuppressWarnings("EjbEnvironmentInspection") @Resource
    private ManagedExecutorService executor;

    @Asynchronous
    public void handle(AsyncContext asyncContext) {
        logger.requestAtScheduler();
        executor.submit(new RequestWorker(asyncContext, backingServer));
    }
}
