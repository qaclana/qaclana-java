/*
 * Copyright 2016 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.api;

import java.util.concurrent.atomic.AtomicReference;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Singleton;

/**
 * Stores the current system state. Consumers can rest assured that the system state on this container will always
 * represent the correct system state, so, stateless or singleton EJBs can, for instance, just inject this container
 * and consume the system state by calling the getter.
 *
 * @author Juraci Paixão Kröhling
 */
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class SystemStateContainer {
    private AtomicReference<SystemState> systemStateAtomicReference = new AtomicReference<>(SystemState.DISABLED);

    public SystemState getState() {
        return systemStateAtomicReference.get();
    }

    public void setState(SystemState state) {
        systemStateAtomicReference.set(state);
    }
}
