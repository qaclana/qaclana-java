/*
 * Copyright 2016 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.filter.control;

import io.qaclana.api.entity.event.AuditEventReported;
import io.qaclana.api.entity.event.SendMessage;
import io.qaclana.api.entity.ws.AuditEventMessage;
import io.qaclana.api.entity.ws.BasicMessage;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.websocket.Session;

/**
 * Emits a {@link SendMessage} based on a {@link AuditEventReported}.
 *
 * @author Juraci Paixão Kröhling
 */
@Stateless
@Asynchronous
public class AuditEventPropagator {

    @Inject
    Event<SendMessage> sendMessageEvent;

    @Inject
    SocketSessionContainer socketSessionContainer;

    public void propagate(@Observes AuditEventReported auditEventReported) {
        BasicMessage message = new AuditEventMessage(auditEventReported.getAudit());
        Session destination = socketSessionContainer.getSession();
        sendMessageEvent.fire(new SendMessage(destination, message));
    }
}
