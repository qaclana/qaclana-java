/*
 * Copyright 2016 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.filter.control;

import java.io.StringReader;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import io.qaclana.api.SystemState;
import io.qaclana.api.SystemStateContainer;
import io.qaclana.api.entity.event.NewSocketMessage;
import io.qaclana.api.entity.event.SystemStateChange;
import io.qaclana.api.entity.event.SystemStateChangeApplied;
import io.qaclana.api.entity.ws.SystemStateChangeMessage;

/**
 * @author Juraci Paixão Kröhling
 */
@Stateless
@Asynchronous
public class SystemStateFilterUpdater {
    private static final MsgLogger logger = MsgLogger.LOGGER;

    @Inject
    SystemStateContainer systemStateInstance;

    @Inject
    Event<SystemStateChange> systemStateChangeEvent;

    @Inject
    Event<SystemStateChangeApplied> systemStateChangeAppliedEvent;

    public void acceptChangeFromSocket(@Observes NewSocketMessage newSocketMessage) {
        if (!SystemStateChangeMessage.EVENT_TYPE.equals(newSocketMessage.getType())) {
            return;
        }

        String message = newSocketMessage.getMessage();
        logger.systemStateChangeMessageReceived(message);

        JsonReader reader = Json.createReader(new StringReader(message));
        JsonObject object = reader.readObject();
        String systemStateFromMessage = object.getString("state");

        SystemState systemState = SystemState.valueOf(systemStateFromMessage);
        systemStateChangeEvent.fire(new SystemStateChange(systemState));
    }

    public void updateSystemState(@Observes SystemStateChange systemStateChange) {
        logger.applyingSystemStateChange(systemStateChange.getState().toString());
        systemStateInstance.setState(systemStateChange.getState());
        systemStateChangeAppliedEvent.fire(new SystemStateChangeApplied(systemStateChange.getState()));
    }
}
