/*
 * Copyright 2016 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.services.jpa.control;

import io.qaclana.api.control.WhitelistService;
import io.qaclana.api.entity.IpRange;
import io.qaclana.api.entity.event.IpRangeAddedToWhitelist;
import io.qaclana.api.entity.event.IpRangeRemovedFromWhitelist;
import io.qaclana.services.jpa.entity.IpRangeEntity;
import io.qaclana.services.jpa.entity.IpRangeEntity_;
import io.qaclana.services.jpa.entity.IpRangeType;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Juraci Paixão Kröhling
 */
@Stateless
public class WhitelistServiceJPA implements WhitelistService {
    private static final MsgLogger logger = MsgLogger.LOGGER;

    @Inject
    EntityManager entityManager;

    @Inject
    Event<IpRangeAddedToWhitelist> ipRangeAddedToWhitelistEvent;

    @Inject
    Event<IpRangeRemovedFromWhitelist> ipRangeRemovedFromWhitelistEvent;

    @Override
    public List<IpRange> list() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<IpRangeEntity> query = builder.createQuery(IpRangeEntity.class);
        Root<IpRangeEntity> root = query.from(IpRangeEntity.class);
        query.select(root);
        query.where(builder.equal(root.get(IpRangeEntity_.ipRangeType), IpRangeType.WHITELIST));

        List<IpRangeEntity> results = entityManager.createQuery(query).getResultList();
        return results.stream().map(IpRangeEntity::toIpRange).collect(Collectors.toList());
    }

    @Override
    public void add(IpRange ipRange) {
        if (!isInWhitelist(ipRange)) {
            entityManager.persist(new IpRangeEntity(ipRange, IpRangeType.WHITELIST));
            ipRangeAddedToWhitelistEvent.fire(new IpRangeAddedToWhitelist(ipRange));
        } else {
            logger.ipRangeAlreadyInWhitelist(ipRange.toString());
        }
    }

    @Override
    public void remove(IpRange ipRange) {
        if (isInWhitelist(ipRange)) {
            entityManager.remove(get(ipRange));
            ipRangeRemovedFromWhitelistEvent.fire(new IpRangeRemovedFromWhitelist(ipRange));
        } else {
            logger.ipRangeNotInWhitelist(ipRange.toString());
        }
    }

    @Override
    public boolean isInWhitelist(IpRange ipRange) {
        return null != get(ipRange);
    }

    private IpRangeEntity get(IpRange ipRange) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<IpRangeEntity> query = builder.createQuery(IpRangeEntity.class);
        Root<IpRangeEntity> root = query.from(IpRangeEntity.class);
        query.select(root);
        query.where(
                builder.equal(root.get(IpRangeEntity_.start), ipRange.getStart()),
                builder.equal(root.get(IpRangeEntity_.end), ipRange.getEnd()),
                builder.equal(root.get(IpRangeEntity_.ipRangeType), IpRangeType.WHITELIST)
        );

        List<IpRangeEntity> results = entityManager.createQuery(query).getResultList();
        if (results.size() == 1) {
            return results.get(0);
        }

        if (results.size() > 1) {
            throw new IllegalStateException("Duplicate IP range found for " + ipRange);
        }

        return null;
    }
}
