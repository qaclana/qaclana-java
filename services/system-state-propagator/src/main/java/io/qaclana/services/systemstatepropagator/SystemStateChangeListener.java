/*
 * Copyright 2016 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.services.systemstatepropagator;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import io.qaclana.api.SystemStateContainer;
import io.qaclana.api.entity.event.SystemStateChange;

/**
 * @author Juraci Paixão Kröhling
 */
@MessageDriven(mappedName="jms/topic/Topic", activationConfig =  {
        @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "topic/SystemStateChangeDestination"),
        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic")
})
public class SystemStateChangeListener implements MessageListener {
    private static final MsgLogger logger = MsgLogger.LOGGER;

    @Inject
    Event<SystemStateChange> systemStateChangeEvent;

    @Inject
    SystemStateContainer systemStateInstance;

    @Override
    public void onMessage(Message message) {
        if (!(message instanceof ObjectMessage)) {
            throw new IllegalStateException("The system change message on the destination is not a ObjectMessage");
        }

        try {
            Object objectMessage = ((ObjectMessage) message).getObject();
            if (!(objectMessage instanceof SystemStateChange)) {
                throw new IllegalStateException("The object in the message is not a SystemStateChange");
            }
            SystemStateChange systemStateChange = (SystemStateChange) objectMessage;

            if (systemStateChange.getState().equals(systemStateInstance.getState())) {
                logger.notApplyingSystemStateChange(systemStateChange.getState().toString());
            } else {
                logger.systemStateChangeMessageReceived(systemStateChange.getState().toString());
                systemStateChangeEvent.fire(systemStateChange);
            }

        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
