/*
 * Copyright 2016 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.services.systemstatepropagator;

import java.util.Map;

import javax.ejb.Asynchronous;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import io.qaclana.api.Cached;
import io.qaclana.api.SystemStateContainer;
import io.qaclana.api.entity.event.SystemStateChange;
import io.qaclana.api.entity.event.SystemStateChangeApplied;

/**
 * @author Juraci Paixão Kröhling
 */
@Singleton
@Startup
public class SystemStateUpdater {
    private static final MsgLogger logger = MsgLogger.LOGGER;

    @Inject
    SystemStateContainer systemStateInstance;

    @Inject
    Event<SystemStateChangeApplied> systemStateChangeAppliedEvent;

    @Inject @Cached
    Map<String, String> cache;

    @Asynchronous
    public void updateSystemState(@Observes SystemStateChange systemStateChange) {
        if (systemStateChange.getState().equals(systemStateInstance.getState())) {
            logger.notApplyingSystemStateChange(systemStateChange.getState().toString());
        } else {
            logger.applyingSystemStateChange(systemStateChange.getState().toString());
            cache.put("system-state", systemStateChange.getState().toString());
            systemStateInstance.setState(systemStateChange.getState());
            systemStateChangeAppliedEvent.fire(new SystemStateChangeApplied(systemStateChange.getState()));
        }
    }
}
