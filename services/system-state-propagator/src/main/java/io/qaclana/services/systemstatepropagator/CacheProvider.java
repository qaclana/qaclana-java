/*
 * Copyright 2016 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.services.systemstatepropagator;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import org.infinispan.manager.EmbeddedCacheManager;
import io.qaclana.api.Cached;

/**
 * @author Juraci Paixão Kröhling
 */
@ApplicationScoped
@Startup
@Singleton
public class CacheProvider {
    private Map<String, String> cache = null;

    @Resource(lookup="java:jboss/infinispan/container/qaclana-system-state")
    private EmbeddedCacheManager systemStateCacheManager;

    @PostConstruct
    public void setCache() {
        cache = systemStateCacheManager.getCache();
    }

    @Produces
    @Cached
    public Map<String, String> getCache() {
        if (null == cache) {
            cache = systemStateCacheManager.getCache();
        }
        return cache;
    }
}
