/*
 * Copyright 2016 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.filter.example;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Juraci Paixão Kröhling
 */
@Path("foo")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ExampleEndpoint {
    private static final List<String> listOfFoos = new ArrayList<>();

    @GET
    public Response list() {
        return Response.ok(listOfFoos).build();
    }

    @POST
    public Response createFoo(FooRequest fooRequest) {
        listOfFoos.add(fooRequest.getFoo());
        return Response.ok(fooRequest.getFoo()).build();
    }

    @DELETE
    public Response deleteFoo(String foo) {
        listOfFoos.remove(foo);
        return Response.accepted().build();
    }

    @PUT
    public Response changeFoo(String foo) {
        listOfFoos.remove(foo);
        listOfFoos.add(foo);
        return Response.ok(foo).build();
    }

    @HEAD
    public Response numberOfFoos() {
        return Response.noContent().header("Number-Of-Foos", listOfFoos.size()).build();
    }
}
