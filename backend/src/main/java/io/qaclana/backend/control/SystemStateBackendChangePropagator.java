/*
 * Copyright 2016 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.backend.control;

import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.jms.Destination;
import javax.jms.JMSContext;
import javax.websocket.Session;

import io.qaclana.api.SystemStateContainer;
import io.qaclana.api.control.SystemStateChangeDestination;
import io.qaclana.api.entity.event.SendMessage;
import io.qaclana.api.entity.event.SystemStateChange;
import io.qaclana.api.entity.ws.BasicMessage;
import io.qaclana.api.entity.ws.SystemStateChangeMessage;
import io.qaclana.backend.entity.event.NewFrontendInstanceRegistered;

/**
 * Propagates a system state change to interested parties.
 *
 * @author Juraci Paixão Kröhling
 */
@Stateless
public class SystemStateBackendChangePropagator {
    private static final MsgLogger log = MsgLogger.LOGGER;

    @Inject
    @Frontend
    Instance<Map<String, Session>> frontendSessionsInstance;

    @Inject
    Event<SendMessage> sendMessageEvent;

    @Inject
    SystemStateContainer systemStateInstance;

    @SuppressWarnings("EjbEnvironmentInspection")
    @Resource
    private ManagedExecutorService executor;

    @Inject
    private JMSContext context;

    @Inject
    @SystemStateChangeDestination
    private Destination destination;

    @Asynchronous
    public void propagate(@Observes SystemStateChange changeEvent) {
        log.propagatingSystemStateChange(changeEvent.getState().name());
        propagateToFrontend(changeEvent);
        propagateToServer(changeEvent);
    }

    @Asynchronous
    private void propagateToFrontend(SystemStateChange changeEvent) {
        BasicMessage frontendMessage = new SystemStateChangeMessage(changeEvent.getState());
        frontendSessionsInstance.get().forEach((sessionId, session) ->
                executor.submit(
                        () -> sendMessageEvent.fire(new SendMessage(session, frontendMessage))
                )
        );
    }

    @Asynchronous
    private void propagateToServer(SystemStateChange changeEvent) {
        // now, let's send to the message to the destination, so that the server(s) can get the info
        context.createProducer().send(destination, changeEvent);
        log.systemStateChangeSentToServer();
    }

    /**
     * Whenever we get a new instance registered, we want to propagate the current state of the system to it.
     *
     * @param newFrontendInstanceRegistered the event with the new session
     */
    @Asynchronous
    public void propagate(@Observes NewFrontendInstanceRegistered newFrontendInstanceRegistered) {
        BasicMessage message = new SystemStateChangeMessage(systemStateInstance.getState());
        executor.submit(
                () -> sendMessageEvent.fire(new SendMessage(newFrontendInstanceRegistered.getSession(), message))
        );
    }
}
