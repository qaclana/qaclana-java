/*
 * Copyright 2016 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.backend.control;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import io.qaclana.api.control.WhitelistService;
import io.qaclana.api.entity.IpRange;
import io.qaclana.api.entity.event.IpRangeAddedToWhitelist;
import io.qaclana.api.entity.event.IpRangeRemovedFromWhitelist;
import io.qaclana.api.entity.event.SendMessage;

/**
 * Propagates the information about the whitelisting of a specific {@link IpRange}.
 *
 * @author Juraci Paixão Kröhling
 */
@Stateless
public class IpRangeWhitelistedPropagator {
    @Inject
    Event<SendMessage> sendMessageEvent;

    @Inject
    WhitelistService whitelistService;

    @SuppressWarnings("EjbEnvironmentInspection")
    @Resource
    private ManagedExecutorService executor;

    /**
     * Builds a new Web Socket message and sends to all firewall instances with open sockets with us. Uses a
     * {@link ManagedExecutorService} to execute the submissions.
     *
     * @param ipRangeAddedToWhitelist the new whitelisted {@link IpRange}
     */
    @Asynchronous
    public void propagate(@Observes IpRangeAddedToWhitelist ipRangeAddedToWhitelist) {
        // TODO: send to topic!
    }

    /**
     * Builds a new Web Socket message and sends to all firewall instances with open sockets with us. Uses a
     * {@link ManagedExecutorService} to execute the submissions.
     *
     * @param ipRangeRemovedFromWhitelist the {@link IpRange} that has been removed from the whitelist
     */
    @Asynchronous
    public void propagate(@Observes IpRangeRemovedFromWhitelist ipRangeRemovedFromWhitelist) {
        // TODO: send to topic!
    }
}
