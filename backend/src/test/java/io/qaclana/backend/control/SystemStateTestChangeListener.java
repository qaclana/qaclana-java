/*
 * Copyright 2016 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.backend.control;

import java.util.concurrent.CountDownLatch;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;

/**
 * @author Juraci Paixão Kröhling
 */
@MessageDriven(mappedName="jms/topic/Topic", activationConfig =  {
        @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "topic/SystemStateChangeDestination"),
        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic")
})
public class SystemStateTestChangeListener implements MessageListener {
    static CountDownLatch jmsMessageLatch; // we expect one message to arrive
    static Message jmsMessage;

    public SystemStateTestChangeListener() {
    }

    @Override
    public void onMessage(Message message) {
        System.out.println("Message received by the test");
        jmsMessage = message;
        jmsMessageLatch.countDown();
    }
}
