/*
 * Copyright 2016 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.backend.control;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.ejb.Singleton;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.JMSDestinationDefinition;
import javax.jms.JMSDestinationDefinitions;
import javax.jms.ObjectMessage;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.ws.rs.core.Response;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import io.qaclana.api.SystemState;
import io.qaclana.api.SystemStateContainer;
import io.qaclana.api.control.SystemStateChangeDestination;
import io.qaclana.api.entity.event.BasicEvent;
import io.qaclana.api.entity.event.NewSocketMessage;
import io.qaclana.api.entity.event.SendMessage;
import io.qaclana.api.entity.event.SystemStateChange;
import io.qaclana.api.entity.event.SystemStateChangeApplied;
import io.qaclana.api.entity.ws.BasicMessage;
import io.qaclana.api.entity.ws.SystemStateChangeMessage;
import io.qaclana.backend.boundary.SystemStateEndpoint;
import io.qaclana.backend.entity.event.NewFrontendInstanceRegistered;
import io.qaclana.backend.entity.rest.ErrorResponse;
import io.qaclana.backend.entity.rest.SystemStateRequest;
import io.qaclana.services.messagesender.SocketMessagePropagator;
import io.qaclana.services.systemstatepropagator.CacheProvider;
import io.qaclana.services.systemstatepropagator.DestinationConfiguration;
import io.qaclana.services.systemstatepropagator.SystemStateUpdater;

/**
 * @author Juraci Paixão Kröhling
 */
@Singleton
@RunWith(Arquillian.class)
public class SystemStateEndpointTest {
    // static because the instance of our test is not the same as the singleton EJB
    private static CountDownLatch cdiEventLatch; // we expect one message to arrive
    private static SystemStateChangeApplied changeEvent;

    @Inject
    SystemStateEndpoint endpoint;

    @Inject
    RunAsAdmin runAsAdmin;

    @Inject
    SystemStateContainer systemStateInstance;

    @Deployment
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class)
                .addClass(BasicEvent.class)
                .addClass(BasicMessage.class)
                .addClass(ErrorResponse.class)
                .addClass(MsgLogger.class)
                .addClass(MsgLogger_$logger.class)
                .addClass(NewFrontendInstanceRegistered.class)
                .addClass(RunAsAdmin.class)
                .addClass(RunAsAdmin.class)
                .addClass(SendMessage.class)
                .addClass(SystemState.class)
                .addClass(SystemStateChange.class)
                .addClass(SystemStateChangeApplied.class)
                .addClass(SystemStateBackendChangePropagator.class)
                .addClass(SystemStateChangeMessage.class)
                .addClass(SystemStateContainer.class)
                .addClass(SystemStateEndpoint.class)
                .addClass(SystemStateRequest.class)
                .addClass(NewSocketMessage.class)
                .addClass(SocketMessagePropagator.class)
                .addClass(JMSContext.class)
                .addClass(SystemStateChangeDestination.class)
                .addClass(Server.class)
                .addClass(JMSDestinationDefinition.class)
                .addClass(JMSDestinationDefinitions.class)
                .addClass(BackendResources.class)
                .addClass(SystemStateTestChangeListener.class)
                .addClass(ApplicationLifecycleListener.class)
                .addClass(ServletContextListener.class)
                .addClass(ServletContextEvent.class)
                .addClass(DestinationConfiguration.class)
                .addClass(SystemStateUpdater.class)
                .addClass(TestCacheProvider.class)
                .addClass(io.qaclana.services.systemstatepropagator.MsgLogger.class)
                .addClass(io.qaclana.services.systemstatepropagator.MsgLogger_$logger.class)
                .addClass(io.qaclana.services.messagesender.MsgLogger.class)
                .addClass(io.qaclana.services.messagesender.MsgLogger_$logger.class)
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Test
    public void changeOfSystemStateIsPropagatedTest() throws Exception {
        // TODO: change this test to use actual web sockets once WFLY-3313 is fixed.
        runAsAdmin.call(() -> {
            cdiEventLatch = new CountDownLatch(1);
            SystemStateTestChangeListener.jmsMessageLatch = new CountDownLatch(1);
            changeEvent = null;
            SystemStateTestChangeListener.jmsMessage = null;

            // ensure we have a sane start
            assertEquals(SystemState.DISABLED, systemStateInstance.getState());

            // we then change the system state
            SystemStateRequest request = new SystemStateRequest();
            request.setState(SystemState.ENFORCING.name());
            endpoint.update(request);

            cdiEventLatch.await(5000, TimeUnit.MILLISECONDS);
            SystemStateTestChangeListener.jmsMessageLatch.await(5000, TimeUnit.MILLISECONDS);

            assertNotNull(changeEvent);
            assertNotNull(SystemStateTestChangeListener.jmsMessage);

            assertEquals(SystemState.ENFORCING, changeEvent.getState());
            assertTrue(SystemStateTestChangeListener.jmsMessage instanceof ObjectMessage);
            ObjectMessage objectMessage = (ObjectMessage) SystemStateTestChangeListener.jmsMessage;

            assertTrue(objectMessage.getObject() instanceof SystemStateChange);
            SystemStateChange systemStateChange = (SystemStateChange) objectMessage.getObject();

            assertEquals(SystemState.ENFORCING, systemStateChange.getState());
            assertEquals(SystemState.ENFORCING, systemStateInstance.getState());

            // now, we revert the system state
            request.setState(SystemState.DISABLED.name());
            endpoint.update(request);
            return null;
        });
    }

    @Test
    public void invalidSystemStateDoesNotChangeSystemState() throws Exception {
        // TODO: change this test to use actual web sockets once WFLY-3313 is fixed.
        runAsAdmin.call(() -> {
            cdiEventLatch = new CountDownLatch(1);
            changeEvent = null;

            // ensure we have a sane start
            assertEquals(SystemState.DISABLED, systemStateInstance.getState());

            // we then change the system state
            SystemStateRequest request = new SystemStateRequest();
            request.setState("INVALID");
            Response response = endpoint.update(request);

            // this should *always* timeout, as we never expect to get an event
            cdiEventLatch.await(5000, TimeUnit.MILLISECONDS);

            assertNull(changeEvent);
            assertTrue(response.getEntity() instanceof ErrorResponse);
            ErrorResponse errorResponse = (ErrorResponse) response.getEntity();
            assertEquals("invalid_system_state", errorResponse.getCode());

            return null;
        });
    }

    public void getMessage(@Observes SystemStateChangeApplied event) {
        changeEvent = event;
        cdiEventLatch.countDown();
    }
}
