/*
 * Copyright 2016 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.server.control;

import org.jboss.logging.Logger;
import org.jboss.logging.annotations.LogMessage;
import org.jboss.logging.annotations.Message;
import org.jboss.logging.annotations.MessageLogger;
import org.jboss.logging.annotations.ValidIdRange;

/**
 * @author Juraci Paixão Kröhling
 */
@MessageLogger(projectCode = "QCLN")
@ValidIdRange(min = 10340, max = 10359)
public interface MsgLogger {
    MsgLogger LOGGER = Logger.getMessageLogger(MsgLogger.class, MsgLogger.class.getPackage().getName());

    @LogMessage(level = Logger.Level.INFO)
    @Message(id = 10340, value = "Web Socket opened for a Firewall worker.")
    void firewallSocketOpened();

    @LogMessage(level = Logger.Level.INFO)
    @Message(id = 10341, value = "A Firewall worker sent a message: [%s]")
    void firewallSocketMessage(String message);

    @LogMessage(level = Logger.Level.INFO)
    @Message(id = 10342, value = "Web Socket closed for a Firewall worker.")
    void firewallSocketClosed();

    @LogMessage(level = Logger.Level.INFO)
    @Message(id = 10343, value = "Propagating system state change. New state: [%s]")
    void propagatingSystemStateChange(String newState);
}
