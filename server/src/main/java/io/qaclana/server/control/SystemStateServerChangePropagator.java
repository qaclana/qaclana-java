/*
 * Copyright 2016 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.server.control;

import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.websocket.Session;

import io.qaclana.api.SystemStateContainer;
import io.qaclana.api.entity.event.SendMessage;
import io.qaclana.api.entity.event.SystemStateChange;
import io.qaclana.api.entity.ws.BasicMessage;
import io.qaclana.api.entity.ws.SystemStateChangeMessage;
import io.qaclana.server.entity.event.NewFirewallInstanceRegistered;

/**
 * Propagates a system state change to interested parties.
 *
 * @author Juraci Paixão Kröhling
 */
@Stateless
public class SystemStateServerChangePropagator {
    private static final MsgLogger log = MsgLogger.LOGGER;

    @Inject
    @Firewall
    Instance<Map<String, Session>> firewallSessionsInstance;

    @Inject
    Event<SendMessage> sendMessageEvent;

    @Inject
    SystemStateContainer systemStateInstance;

    @SuppressWarnings("EjbEnvironmentInspection")
    @Resource
    private ManagedExecutorService executor;

    @Asynchronous
    public void propagate(@Observes SystemStateChange changeEvent) {
        log.propagatingSystemStateChange(changeEvent.getState().name());
        BasicMessage message = new SystemStateChangeMessage(changeEvent.getState());
        propagateTo(firewallSessionsInstance.get(), message);
    }

    /**
     * Whenever we get a new instance registered, we want to propagate the current state of the system to it.
     *
     * @param newFirewallInstanceRegistered the event with the new session
     */
    @Asynchronous
    public void propagate(@Observes NewFirewallInstanceRegistered newFirewallInstanceRegistered) {
        BasicMessage message = new SystemStateChangeMessage(systemStateInstance.getState());
        executor.submit(
                () -> sendMessageEvent.fire(new SendMessage(newFirewallInstanceRegistered.getSession(), message))
        );
    }

    private void propagateTo(Map<String, Session> sessions, BasicMessage message) {
        sessions.forEach((sessionId, session) ->
                executor.submit(
                        () -> sendMessageEvent.fire(new SendMessage(session, message))
                )
        );
    }
}
