/*
 * Copyright 2016 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.server.control;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import java.io.File;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.ejb.Singleton;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.websocket.Session;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;
import io.qaclana.api.SystemState;
import io.qaclana.api.SystemStateContainer;
import io.qaclana.api.entity.event.BasicEvent;
import io.qaclana.api.entity.event.IpRangeRemovedFromBlacklist;
import io.qaclana.api.entity.event.NewSocketMessage;
import io.qaclana.api.entity.event.SendMessage;
import io.qaclana.api.entity.event.SystemStateChange;
import io.qaclana.api.entity.ws.BasicMessage;
import io.qaclana.api.entity.ws.SystemStateChangeMessage;
import io.qaclana.server.boundary.FirewallSocket;
import io.qaclana.server.entity.event.NewFirewallInstanceRegistered;
import io.qaclana.services.messagesender.SocketMessagePropagator;

/**
 * @author Juraci Paixão Kröhling
 */
@Singleton
@RunWith(Arquillian.class)
public class NewFirewallInstanceReceivesSystemStateTest {
    // static because the instance of our test is not the same as the singleton EJB
    private static CountDownLatch latch = new CountDownLatch(1); // we expect one message to arrive
    private static SendMessage receivedMessage;

    @Inject
    Event<NewFirewallInstanceRegistered> newFirewallInstanceRegisteredEvent;

    @Inject
    FirewallSocket firewallSocket;

    @Inject
    @Firewall
    private Instance<Map<String, Session>> sessionsInstance;

    @Deployment
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class)
                .addClass(ServerResources.class)
                .addClass(BasicMessage.class)
                .addClass(BasicEvent.class)
                .addClass(Firewall.class)
                .addClass(FirewallSocket.class)
                .addClass(MsgLogger.class)
                .addClass(MsgLogger_$logger.class)
                .addClass(NewFirewallInstanceRegistered.class)
                .addClass(IpRangeRemovedFromBlacklist.class)
                .addClass(SendMessage.class)
                .addClass(SystemState.class)
                .addClass(SystemStateServerChangePropagator.class)
                .addClass(SystemStateChange.class)
                .addClass(SystemStateChangeMessage.class)
                .addClass(SystemStateContainer.class)
                .addClass(SocketMessagePropagator.class)
                .addClass(NewSocketMessage.class)
                .addClass(io.qaclana.services.messagesender.MsgLogger.class)
                .addClass(io.qaclana.services.messagesender.MsgLogger_$logger.class)
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsLibraries(Maven.resolver().resolve("org.mockito:mockito-all:1.10.19").withoutTransitivity().as(File.class));
    }

    @Test
    public void newInstanceIsRegisteredTest() throws Exception {
        // TODO: change this test to use actual web sockets once WFLY-3313 is fixed.
        assertEquals(0, sessionsInstance.get().size());
        Session session = mock(Session.class);
        firewallSocket.onOpen(session);

        latch.await(2, TimeUnit.SECONDS);

        assertEquals(1, sessionsInstance.get().size());
        assertTrue(receivedMessage.getMessage() instanceof SystemStateChangeMessage);
        SystemStateChangeMessage message = (SystemStateChangeMessage) receivedMessage.getMessage();
        assertNotNull(message.getState());
    }

    public void getMessage(@Observes SendMessage event) {
        receivedMessage = event;
        latch.countDown();
    }
}
