/*
 * Copyright 2016 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.server.control;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.jms.Destination;
import javax.jms.JMSContext;
import javax.jms.JMSDestinationDefinition;
import javax.jms.JMSDestinationDefinitions;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import io.qaclana.api.SystemState;
import io.qaclana.api.SystemStateContainer;
import io.qaclana.api.entity.event.BasicEvent;
import io.qaclana.api.entity.event.SystemStateChange;
import io.qaclana.services.systemstatepropagator.*;

/**
 * @author Juraci Paixão Kröhling
 */
@Singleton
@RunWith(Arquillian.class)
public class PublishedSystemStateIsPropagated {
    private static CountDownLatch latch;
    private static SystemStateChange changeEvent;

    @Resource(lookup = "java:/topic/SystemStateChangeDestination")
    private Destination destination;

    @Inject
    private JMSContext context;

    @Deployment
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class)
                .addClass(ServerResources.class)
                .addClass(BasicEvent.class)
                .addClass(SystemState.class)
                .addClass(SystemStateChange.class)
                .addClass(SystemStateChangeListener.class)
                .addClass(SystemStateContainer.class)

                .addClass(ServletContextListener.class)
                .addClass(ServletContextEvent.class)
                .addClass(JMSDestinationDefinition.class)
                .addClass(JMSDestinationDefinitions.class)
                .addClass(DestinationConfiguration.class)
                .addClass(io.qaclana.services.systemstatepropagator.MsgLogger.class)
                .addClass(io.qaclana.services.systemstatepropagator.MsgLogger_$logger.class)
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Test
    public void eventIsPropagated() throws Exception {
        latch = new CountDownLatch(1);
        SystemStateChange systemStateChange = new SystemStateChange(SystemState.ENFORCING);
        context.createProducer().send(destination, systemStateChange);
        latch.await(1_000, TimeUnit.MILLISECONDS);
        assertEquals(changeEvent.getState(), SystemState.ENFORCING);
    }

    public void observeEvent(@Observes SystemStateChange systemStateChange) {
        changeEvent = systemStateChange;
        latch.countDown();
    }

}
